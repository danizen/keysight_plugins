package com.keysight.database.macros;

import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.keysight.database.helpers.PluginConfigManager;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;

public class DatabaseQueryCompact extends DatabaseQuery {

    public DatabaseQueryCompact( CacheManager cacheManager,
                                 PageManager pageManager,
                                 PluginConfigManager pluginConfigManager,
                                 PluginSettingsFactory pluginSettingsFactory,
                                 SettingsManager settingsManager,
                                 SpaceManager spaceManager,
                                 MultiQueueTaskManager taskManager,
                                 TransactionTemplate transactionTemplate,
                                 VelocityHelperService velocityHelperService
    ) {
        super( cacheManager, pageManager, pluginConfigManager, pluginSettingsFactory,
               settingsManager, spaceManager, taskManager, transactionTemplate, velocityHelperService);
    }

    public BodyType getBodyType() { return BodyType.NONE; }
}
