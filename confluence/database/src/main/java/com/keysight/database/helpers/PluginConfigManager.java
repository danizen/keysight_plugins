package com.keysight.database.helpers;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class PluginConfigManager {
    // note, may need to load jdb2jcc.jar and db2jcc_license_cu.jar
    private final String DB2_DOWNLOAD = null;
    private final String DB2_DRIVER_KEY = "DB2";
    private final String DB2_REGEX = ".*db2jcc.*";
    private final String DB2_CLASS_NAME = "com.ibm.db2.jcc.DB2Driver";

    private String DERBY_DOWNLOAD = "http://central.maven.org/maven2/org/apache/derby/derby/10.12.1.1/derby-10.12.1.1.jar";
    private final String DERBY_DRIVER_KEY = "Derby";
    private final String DERBY_REGEX = ".*derby.*";
    private final String DERBY_CLASS_NAME = "org.apache.derby.jdbc.EmbeddedDriver";

    private String JTDS_SQL_SERVER_DOWNLOAD = "http://central.maven.org/maven2/net/sourceforge/jtds/jtds/1.3.1/jtds-1.3.1.jar";
    private final String JTDS_SQL_SERVER_DRIVER_KEY = "jTDS SQL Server";
    private final String JTDS_SQL_SERVER_REGEX = ".*jtds.*";
    private final String JTDS_SQL_SERVER_CLASS_NAME = "net.sourceforge.jtds.jdbc.Driver";

    private String SQL_SERVER_DOWNLOAD = "http://central.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/6.1.0.jre8/mssql-jdbc-6.1.0.jre8.jar";
    private final String SQL_SERVER_DRIVER_KEY = "Microsoft SQL Server";
    private final String SQL_SERVER_REGEX = ".*sqljdbc.*";
    private final String SQL_SERVER_CLASS_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    private String MONGODB_DOWNLOAD = "http://central.maven.org/maven2/org/mongodb/mongodb-driver/3.4.2/mongodb-driver-3.4.2.jar";
    private final String MONGODB_DRIVER_KEY = "MongoDB";
    private final String MONGODB_REGEX = ".*mongodb.*";
    private final String MONGODB_CLASS_NAME = "mongodb.jdbc.MongoDriver";

    private String MYSQL_DOWNLOAD = "http://central.maven.org/maven2/mysql/mysql-connector-java/6.0.6/mysql-connector-java-6.0.6.jar";
    private final String MYSQL_DRIVER_KEY = "MySQL";
    private final String MYSQL_REGEX = ".*mysql.*";
    private final String MYSQL_CLASS_NAME = "com.mysql.jdbc.Driver";

    private final String ORACLE_DOWNLOAD = null;
    private final String ORACLE_DRIVER_KEY = "Oracle";
    private final String ORACLE_REGEX = ".*ojdbc.*";
    private final String ORACLE_CLASS_NAME = "oracle.jdbc.driver.OracleDriver";

    private String POSTGRESQL_DOWNLOAD = "https://jdbc.postgresql.org/download/postgresql-42.1.1.jar";
    private final String POSTGRESQL_DRIVER_KEY = "PostgreSQL";
    private final String POSTGRESQL_REGEX = ".*postgre.*";
    private final String POSTGRESQL_CLASS_NAME = "org.postgresql.Driver";

    private final String SYBASE_DOWNLOAD = null;
    private final String SYBASE_DRIVER_KEY = "Sybase";
    private final String SYBASE_REGEX = ".*jconn.*";
    private final String SYBASE_CLASS_NAME = "sybase.jdbc.sqlanywhere.IDriver";

    private String jdbcDriverDirectory;
    private String configAuthorizedUsers;
    private String configAuthorizedGroups;
    private String rowLimitType;
    private String rowLimitValue;
    private String timeoutLimitType;
    private String timeoutLimitValue;
    private String notificationEmail;
    private String atlassianLogLevel;
    private String logEmail;
    private String emailContentFormat = "";
    private String auditLogDbProfile = "";
    private String logEntryTableName = "";
    private String logEntryLifeTime = "";
    private String maxCacheLifeTimeInDays = "7";

    private Element generalConfigRoot;
    private Element profileConfigRoot;

    private String profileConfigXml;
    private String generalConfigXml;

    private Map<String, String> passwordMap;
    private static final Logger log = LoggerFactory.getLogger(PluginConfigManager.class);

    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;

    private final static String INFO = "Info";
    private final static String SOFT = "Soft";
    private final static String PLAIN_TEXT = "Plain Text";
    private final static String NEW_LOG_ENTRY = "newLogEntry";

    @Autowired
    public PluginConfigManager(final PluginSettingsFactory pluginSettingsFactory,
                               final TransactionTemplate transactionTemplate) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.loadFromStorage();
    }

    public void loadFromStorage()
    {
        DocumentBuilder builder;
        Document document;
        try {
            this.loadGeneralConfigXmlFromStorage();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(generalConfigXml)));
            generalConfigRoot = document.getDocumentElement();

            this.loadProfileConfigXmlFromStorage();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(profileConfigXml)));
            profileConfigRoot = document.getDocumentElement();

            this.loadPasswordMapFromStorage();

            jdbcDriverDirectory = new String(Base64.decodeBase64(getValueOfElement("jdbc-driver-directory")), "UTF-8");

            String jTdsDriverUrl = new String(Base64.decodeBase64(getValueOfElement("jtds-driver-url")), "UTF-8");
            String microsoftDriverUrl = new String(Base64.decodeBase64(getValueOfElement("microsoft-driver-url")), "UTF-8");
            String derbyDriverUrl = new String(Base64.decodeBase64(getValueOfElement("derby-driver-url")), "UTF-8");
            String mongoDriverUrl = new String(Base64.decodeBase64(getValueOfElement("mongo-driver-url")), "UTF-8");
            String mysqlDriverUrl = new String(Base64.decodeBase64(getValueOfElement("mysql-driver-url")), "UTF-8");
            String pgsqlDriverUrl = new String(Base64.decodeBase64(getValueOfElement("pgsql-driver-url")), "UTF-8");

            if (!StringUtils.isEmpty(jTdsDriverUrl)){
                JTDS_SQL_SERVER_DOWNLOAD = jTdsDriverUrl;
            }
            if (!StringUtils.isEmpty(microsoftDriverUrl)){
                SQL_SERVER_DOWNLOAD = microsoftDriverUrl;
            }
            if (!StringUtils.isEmpty(derbyDriverUrl)){
                DERBY_DOWNLOAD = derbyDriverUrl;
            }
            if (!StringUtils.isEmpty(mongoDriverUrl)){
                MONGODB_DOWNLOAD = mongoDriverUrl;
            }
            if (!StringUtils.isEmpty(mysqlDriverUrl)){
                MYSQL_DOWNLOAD = mysqlDriverUrl;
            }
            if (!StringUtils.isEmpty(pgsqlDriverUrl)){
                POSTGRESQL_DOWNLOAD = pgsqlDriverUrl;
            }

            configAuthorizedUsers = new String(Base64.decodeBase64(getValueOfElement("config-authorized-users")), "UTF-8");
            configAuthorizedGroups = new String(Base64.decodeBase64(getValueOfElement("config-authorized-groups")), "UTF-8");
            rowLimitType = new String(Base64.decodeBase64(getValueOfElement("row-limit-type")), "UTF-8");
            rowLimitValue = new String(Base64.decodeBase64(getValueOfElement("row-limit-value")), "UTF-8");
            timeoutLimitType = new String(Base64.decodeBase64(getValueOfElement("timeout-limit-type")), "UTF-8");
            timeoutLimitValue = new String(Base64.decodeBase64(getValueOfElement("timeout-limit-value")), "UTF-8");
            notificationEmail = new String(Base64.decodeBase64(getValueOfElement("notification-email")), "UTF-8");

            atlassianLogLevel = new String(Base64.decodeBase64(getValueOfElement("atlassian-log-level")), "UTF-8");
            logEmail = new String(Base64.decodeBase64(getValueOfElement("log-email")), "UTF-8");
            emailContentFormat = new String(Base64.decodeBase64(getValueOfElement("email-content-format")), "UTF-8");
            auditLogDbProfile = new String(Base64.decodeBase64(getValueOfElement("audit-log-db-profile")), "UTF-8");
            logEntryTableName = new String(Base64.decodeBase64(getValueOfElement("log-entry-table-name")), "UTF-8");
            logEntryLifeTime = new String(Base64.decodeBase64(getValueOfElement("log-entry-life-time")), "UTF-8");
            maxCacheLifeTimeInDays = new String(Base64.decodeBase64(getValueOfElement("max-cache-lifetime-in-days")), "UTF-8");

            if (StringUtils.isEmpty(emailContentFormat)) {
                emailContentFormat = PLAIN_TEXT;
            }

            if (StringUtils.isEmpty(logEntryTableName)) {
                logEntryTableName = NEW_LOG_ENTRY;
            }

            if (StringUtils.isEmpty(atlassianLogLevel)) {
                logEntryTableName = INFO;
            }

            if (StringUtils.isEmpty(timeoutLimitType)) {
                timeoutLimitType = SOFT;
            }

            if (StringUtils.isEmpty(rowLimitType)) {
                rowLimitType = SOFT;
            }

            if (StringUtils.isEmpty(maxCacheLifeTimeInDays)) {
                maxCacheLifeTimeInDays = "7";
            }
        } catch (Exception exception) {
            log.warn("Unable to load the DB Connector plugin configuration:" + exception);
        }
    }

    private void loadGeneralConfigXmlFromStorage()
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                   + "<plugin-configuration>\n"
                   + "    <jtds-driver-url></jtds-driver-url>\n"
                   + "    <microsoft-driver-url></microsoft-driver-url>\n"
                   + "    <derby-driver-url></derby-driver-url>\n"
                   + "    <mongo-driver-url></mongo-driver-url>\n"
                   + "    <mysql-driver-url></mysql-driver-url>\n"
                   + "    <pgsql-driver-url></pgsql-driver-url>\n"
                   + "    <jdbc-driver-directory></jdbc-driver-directory>\n"
                   + "    <config-authorized-users></config-authorized-users>\n"
                   + "    <config-authorized-groups></config-authorized-groups>\n"
                   + "    <row-limit-type></row-limit-type>\n"
                   + "    <row-limit-value></row-limit-value>\n"
                   + "    <timeout-limit-type></timeout-limit-type>\n"
                   + "    <timeout-limit-value></timeout-limit-value>\n"
                   + "    <notification-email></notification-email>\n"
                   + "    <atlassian-log-level></atlassian-log-level>\n"
                   + "    <log-email></log-email>\n"
                   + "    <email-content-format></email-content-format>\n"
                   + "    <audit-log-db-profile></audit-log-db-profile>\n"
                   + "    <log-entry-table-name></log-entry-table-name>\n"
                   + "    <log-entry-life-time></log-entry-life-time>\n"
                   + "</plugin-configuration>\n";

        try {
            PluginConfigContainer pluginConfigContainer = (PluginConfigContainer) transactionTemplate.execute((TransactionCallback<Object>) () -> {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                PluginConfigContainer innerPluginConfigContainer = new PluginConfigContainer();
                innerPluginConfigContainer.setXml((String) settings.get(PluginConfigManager.class.getName() + ".xml"));
                return innerPluginConfigContainer;
            });

            if (pluginConfigContainer.getXml() != null && !StringUtils.isEmpty( pluginConfigContainer.getXml() )) {
                xml = pluginConfigContainer.getXml();
            }
        }
        catch( Exception exception )
        {
            log.warn( "Failed to retrieve or parse the profile xml config: " + exception.getMessage() );
        }

        this.generalConfigXml = xml;
    }

    public boolean setGeneralConfigXml( String xml )
    {
        boolean updated = false;
        if( this.generalConfigXmlIsValid( xml ) ) {
            updated = true;
            transactionTemplate.execute(() -> {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(PluginConfigManager.class.getName() + ".xml", xml);
                return null;
            });
            this.loadFromStorage();
        }
        return updated;
    }

    public String getGeneralConfigXml()
    {
        return generalConfigXml;
    }

    private boolean generalConfigXmlIsValid( String xml )
    {
        boolean flag = true;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element root = document.getDocumentElement();
        }
        catch( Exception exception )
        {
            flag = false;
        }
        return flag;
    }

    private void loadProfileConfigXmlFromStorage()
    {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><saved-profiles></saved-profiles>\n";

        try {
            PluginConfigContainer pluginConfigContainer = (PluginConfigContainer) transactionTemplate.execute((TransactionCallback<Object>) () -> {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                PluginConfigContainer innerPluginConfigContainer = new PluginConfigContainer();
                innerPluginConfigContainer.setXml((String) settings.get(PluginConfigManager.class.getName() + "_profiles.xml"));
                return innerPluginConfigContainer;
            });

            if (pluginConfigContainer.getXml() != null && !StringUtils.isEmpty( pluginConfigContainer.getXml() )) {
                xml = pluginConfigContainer.getXml();
            }
        }
        catch( Exception exception )
        {
            log.warn( "Failed to retrieve or parse the profile xml config: " + exception.getMessage() );
        }

        this.profileConfigXml = xml;
    }

    public boolean setProfileConfigXml( String xml )
    {
        return this.setProfileConfigXml( xml, new HashMap<String,String>() );
    }

    public boolean setProfileConfigXml( String xml, Map<String,String> passwordMap )
    {
        boolean updated = false;

        if( this.profileXmlIsValid( xml ) ) {
            this.profileConfigXml = xml;
            updated = true;

            transactionTemplate.execute(() -> {
                if (passwordMap != null) {
                    storePassword(passwordMap);
                }

                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(PluginConfigManager.class.getName() + "_profiles.xml", xml);
                return null;
            });
            this.loadFromStorage();
        }

        return updated;
    }

    public String getProfileConfigXml()
    {
        return profileConfigXml;
    }

    private boolean profileXmlIsValid( String xml )
    {
        boolean flag = true;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element root = document.getDocumentElement();
        }
        catch( Exception exception )
        {
            flag = false;
        }
        return flag;
    }

    private void loadPasswordMapFromStorage()
    {
        Map<String, String> passwordMap = new HashMap<String, String>();

        try {
            passwordMap = transactionTemplate.execute((TransactionCallback<Map<String, String>>) () -> {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                HashMap<String, String> innerPasswordMap = (HashMap<String, String>) settings.get(PluginConfigManager.class.getName() + "_passwordMap");
                if (innerPasswordMap == null) {
                    innerPasswordMap = new HashMap<String, String>();
                }
                return innerPasswordMap;
            });
        }
        catch( Exception exception )
        {
            log.warn( "Failed to retrieve or parse the profile xml config: " + exception.getMessage() );
        }

        this.passwordMap = passwordMap;
    }

    private void storePassword(Map<String, String> passwords) {
        transactionTemplate.execute((TransactionCallback<Object>)()->{
            PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
            HashMap<String, String> tempPasswordMap = (HashMap<String, String>) settings.get(PluginConfigManager.class.getName() + "_passwordMap");
            if (tempPasswordMap == null) {
                tempPasswordMap = new HashMap<String,String>();
            }

            final HashMap<String, String>innerPasswordMap = tempPasswordMap;

            passwords.forEach((profileId, password) -> {
                if (innerPasswordMap.containsKey(profileId)) {
                    innerPasswordMap.remove(profileId);
                }

                innerPasswordMap.put(profileId, password);
            });

            settings.put(PluginConfigManager.class.getName() + "_passwordMap", innerPasswordMap);
            return innerPasswordMap;
        });
        this.loadPasswordMapFromStorage();
    }

    private void purgePassword(Map<String, String> passwords) {
        transactionTemplate.execute((TransactionCallback<Object>)()->{
            PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
            HashMap<String, String> tempPasswordMap = (HashMap<String, String>) settings.get(PluginConfigManager.class.getName() + "_passwordMap");
            if (tempPasswordMap == null) {
                tempPasswordMap = new HashMap<>();
            }

            final HashMap<String, String> innerPasswordMap = tempPasswordMap;

            passwords.forEach((profileId, password) -> {
                if (innerPasswordMap.containsKey(profileId)) {
                    innerPasswordMap.remove(profileId);
                }
            });

            settings.put(PluginConfigManager.class.getName() + "_passwordMap", innerPasswordMap);
            return innerPasswordMap;
        });
        this.loadPasswordMapFromStorage();
    }

    public List<ConnectionProfile> getConnectionProfiles( )
    {
        NodeList profileNodes = this.getProfiles();
        ArrayList<ConnectionProfile> profiles = new ArrayList<ConnectionProfile>();

        if( profileNodes != null ) {
            for (int i = 0; i < profileNodes.getLength(); i++) {
                Element profileNode = (Element) profileNodes.item(i);
                profiles.add(new ConnectionProfile(profileNode));
            }
        }

        return profiles;
    }

    public ConnectionProfile getConnectionProfileFromId( String profileId )
    {
        NodeList profileNodes = this.getProfiles();
        ConnectionProfile theSelectedProfile = null;

        for (int i = 0; i < profileNodes.getLength(); i++) {
            Element profileNode = (Element) profileNodes.item(i);
            ConnectionProfile profile = new ConnectionProfile( profileNode );
            if( profile.getId() != null && profile.getId().equals(profileId)) {
                theSelectedProfile = profile;
                break;
            }
        }

        return theSelectedProfile;
    }

    private String getValueOfElement(String tagName) {
        NodeList allNodes = generalConfigRoot.getElementsByTagName(tagName);
        String text = null;
        for (int i = 0; i < allNodes.getLength(); i++) {
            if (allNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                text = allNodes.item(i).getTextContent();
                break;
            }
        }
        return text;
    }

    private void printLoadedDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver jdbcDriver = drivers.nextElement();
            if (jdbcDriver.getClass() == DatabaseDriverDelegate.class) {
                DatabaseDriverDelegate delegateDriver = (DatabaseDriverDelegate) drivers.nextElement();
                jdbcDriver = delegateDriver.getRealDriver();
            }
            log.warn("Loaded JDBC Driver: " + jdbcDriver.getClass().getName());
        }
    }

    DriverDetails getDriverDetails(String driverKey) {
        DriverDetails driverDetails = null;

        if (driverKey.equals(MYSQL_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(MYSQL_DOWNLOAD, MYSQL_DRIVER_KEY, MYSQL_REGEX);
            driverDetails.className = MYSQL_CLASS_NAME;
        } else if (driverKey.equals(JTDS_SQL_SERVER_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(JTDS_SQL_SERVER_DOWNLOAD, JTDS_SQL_SERVER_DRIVER_KEY, JTDS_SQL_SERVER_REGEX);
            driverDetails.className = JTDS_SQL_SERVER_CLASS_NAME;
        } else if (driverKey.equals(SQL_SERVER_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(SQL_SERVER_DOWNLOAD, SQL_SERVER_DRIVER_KEY, SQL_SERVER_REGEX);
            driverDetails.className = SQL_SERVER_CLASS_NAME;
        } else if (driverKey.equals(ORACLE_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(ORACLE_DOWNLOAD, ORACLE_DRIVER_KEY, ORACLE_REGEX);
            driverDetails.className = ORACLE_CLASS_NAME;
        } else if (driverKey.equals(POSTGRESQL_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(POSTGRESQL_DOWNLOAD, POSTGRESQL_DRIVER_KEY, POSTGRESQL_REGEX);
            driverDetails.className = POSTGRESQL_CLASS_NAME;
        } else if (driverKey.equals(DB2_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(DB2_DOWNLOAD, DB2_DRIVER_KEY, DB2_REGEX);
            driverDetails.className = DB2_CLASS_NAME;
        } else if (driverKey.equals(DERBY_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(DERBY_DOWNLOAD, DERBY_DRIVER_KEY, DERBY_REGEX);
            driverDetails.className = DERBY_CLASS_NAME;
        } else if (driverKey.equals(MONGODB_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(MONGODB_DOWNLOAD, MONGODB_DRIVER_KEY, MONGODB_REGEX);
            driverDetails.className = MONGODB_CLASS_NAME;
        } else if (driverKey.equals(SYBASE_DRIVER_KEY)) {
            driverDetails = getJdbcDriver(SYBASE_DOWNLOAD, SYBASE_DRIVER_KEY, SYBASE_REGEX);
            driverDetails.className = SYBASE_CLASS_NAME;
        }

        return driverDetails;
    }

    public void loadJdbcDriver(String driverKey) {
        DriverDetails driverDetails = getDriverDetails(driverKey);
        try {
            if (!StringUtils.isEmpty(driverDetails.className)) {
                URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{driverDetails.url}, System.class.getClassLoader());
                Driver driver = (Driver) Class.forName(driverDetails.className, true, urlClassLoader).newInstance();
                DriverManager.registerDriver(new DatabaseDriverDelegate(driver));
            }
            //printLoadedDrivers();
        } catch (Exception exception) {
            log.warn( "Error getting jdbc driver for " + driverKey + ": "+ exception.getMessage() );
        }
    }

    private DriverDetails getJdbcDriver(String url, String driverKey, String regex) {
        DriverDetails driverDetails = new DriverDetails();
        Path jdbcDriverDirectoryPath;

        driverDetails.isLocal = false;
        if (!StringUtils.isEmpty(url)) {
            try {
                driverDetails.url = new URL(url);
                driverDetails.isOk = true;
                //System.out.println("url did parse: " + url);
            } catch (Exception exception) {
                driverDetails.url = null;
                driverDetails.isOk = false;
                //System.out.println("url did not parse: " + url + " " + exception);
            }
        } else {
            driverDetails.url = null;
            driverDetails.isOk = false;
            //System.out.println("no url");
        }

        if (!StringUtils.isEmpty(getJdbcDriverDirectory())) {
            jdbcDriverDirectoryPath = Paths.get(this.getJdbcDriverDirectory());
            //System.out.println( "Driver Directory: " + this.getJdbcDriverDirectory() );
            if (Files.isDirectory(jdbcDriverDirectoryPath)) {
                try {
                    // first look for any jar file in the child directory identifyed by the driverKey
                    jdbcDriverDirectoryPath = Paths.get(this.getJdbcDriverDirectory(), driverKey);
                    if (Files.isDirectory(jdbcDriverDirectoryPath)) {
                        DirectoryStream<Path> directoryStream = Files.newDirectoryStream(jdbcDriverDirectoryPath);
                        for (Path path : directoryStream) {
                            if (path.toString().matches(".*\\.jar")) {
                                driverDetails.isLocal = true;
                                driverDetails.url = path.toUri().toURL();
                                driverDetails.isOk = true;
                                break;
                            }
                        }
                    }
                } catch (Exception exception) {
                }

                // if not found, look for any jar file in the jdbc driver directory matching the regex
                if (!driverDetails.isLocal) {
                    jdbcDriverDirectoryPath = Paths.get(this.getJdbcDriverDirectory());
                    try {
                        DirectoryStream<Path> directoryStream = Files.newDirectoryStream(jdbcDriverDirectoryPath);
                        for (Path path : directoryStream) {
                            if (path.toString().matches(regex)) {
                                driverDetails.isLocal = true;
                                driverDetails.url = path.toUri().toURL();
                                driverDetails.isOk = true;
                                break;
                            }
                        }
                    } catch (Exception exception) {
                    }
                }
            }
        }

        return driverDetails;
    }

    public NodeList getProfiles()
    {
        if( profileConfigRoot != null ) {
            return profileConfigRoot.getElementsByTagName("saved-profile");
        } else {
            log.warn( "No profileConfigRoot: The DB Connector failed to parse the profiles xml config file.");
            return null;
        }
    }

    private String getJdbcDriverDirectory() {
        return this.jdbcDriverDirectory;
    }

    private void setJdbcDriverDirectory(String jdbcDriverDirectory) {
        this.jdbcDriverDirectory = jdbcDriverDirectory;
    }
    public String getRowLimitType() { return rowLimitType; }
    public String getRowLimitValue() { return rowLimitValue; }
    public String getTimeoutLimitType() { return timeoutLimitType; }
    public String getTimeoutLimitValue() { return timeoutLimitValue; }
    public String getNotificationEmail() { return notificationEmail; }
    public String getLogEmail() { return logEmail; }
    public String getEmailContentFormat() { return emailContentFormat; }
    public String getAuditLogDbProfile() { return auditLogDbProfile; }
    public String getLogEntryTableName() { return logEntryTableName; }
    public String getLogEntryLifeTime() { return logEntryLifeTime; }
    public String getMaxCacheLifeTimeInDays() { return maxCacheLifeTimeInDays; }
    public String getAtlassianLogLevel() { return atlassianLogLevel; }

    public long getMaxCacheLifeTimeInDaysAsLong() {
        long lifeTime = 7;
        try{
            lifeTime = Long.parseLong( maxCacheLifeTimeInDays );
        }
        catch( Exception exception )
        {
        }
        return lifeTime;
    }

    // Get authorized user or group by splitting the xml attribute on commas

    public ArrayList<String> getAuthorizedUsers() {
        return new ArrayList<>(Arrays.asList(StringUtils.split(this.configAuthorizedUsers, ",")));
    }

    public ArrayList<String> getAuthorizedGroups() {
        return new ArrayList<>(Arrays.asList(StringUtils.split(this.configAuthorizedGroups, ",")));
    }

    public Map<String, String> getAuthMap() {
        return passwordMap;
    }
}


