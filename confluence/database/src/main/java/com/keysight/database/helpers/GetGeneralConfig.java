package com.keysight.database.helpers;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;

public class GetGeneralConfig extends GetConfig {

   public GetGeneralConfig(LoginUriProvider loginUriProvider,
                           PluginConfigManager pluginConfigManager,
                           PluginSettingsFactory pluginSettingsFactory,
                           TransactionTemplate transactionTemplate,
                           UserManager userManager) {
       super( loginUriProvider, pluginConfigManager, pluginSettingsFactory, transactionTemplate, userManager );
   }

   @Override
   protected String getFileName()
   {
      return "dbConnectorGeneralConfig.xml";
   }

   @Override
   protected String getXml()
   {
       // needed for Confluence Data Center as the config could have been
       // updated by an instance of Confluence other than this one.
       pluginConfigManager.loadFromStorage();
       return pluginConfigManager.getGeneralConfigXml();
   }
}
