databaseConfigHelper = (function($) {

    var failedToParseSavedData = false;
    var methods = new Object();
    var url = AJS.contextPath() + "/rest/database/1.0/admin-config/configuration";
    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.1.0";
    var schemaVersionArr = schemaVersionString.split(".");

    methods['canParseSavedResults'] = function(){
       var bFlag = false;
       try {
          var testXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                      + "<root><version>1.0.0</version></root>"
          var $xml = $($.parseXML(testXml));
          var version = $xml.find("version").html();
          if( version === "1.0.0" ){
             failedToParseSavedData = true;
             bFlag = true;
          }
       } catch( exception ){
       }
       return bFlag;
    }

    methods['saveJdbcDriverDirectory'] = function() {
        if( failedToParseSavedData ){
           return true;
        }
        saveConfig();
        location.reload();
    }

    methods['saveConfigAuth'] = function() {
        if( failedToParseSavedData ){
           return true;
        }
        saveConfig();
    }

    methods['addGroup'] = function() {
        if( failedToParseSavedData ){
           return true;
        }
        if (AJS.$("#group-list").length == 0) {
            AJS.$("#group-list-container").html("<ul id=\"group-list\"></ul>");
        }

        appendGroup(AJS.$("#new-group-name").val());
        AJS.$("#new-group-name").val("");

        saveConfig();
    }

    methods['removeEntry'] = function(e) {
        e.preventDefault();
        if( failedToParseSavedData ){
           return true;
        }
        $(e.currentTarget).parent().remove();

        if (AJS.$(".saved-group").length == 0) {
            AJS.$("#group-list-container").html("<div class=\"none-saved\">No saved groups</div>");
        }

        if (AJS.$(".saved-address").length == 0) {
            AJS.$("#email-list-container").html("<div class=\"none-saved\">No saved addresses.</div>");
        }

        saveConfig();
    }

    methods['loadConfig'] = function() {
        $.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            try{
                failedToParseSavedData = false;
                var defaultSchema = "<plugin-configuration>\n"
                                  + "    <jdbc-driver-directory></jdbc-driver-directory>\n"
                                  + "    <jtds-driver-url></jtds-driver-url>\n"
                                  + "    <microsoft-driver-url></microsoft-driver-url>\n"
                                  + "    <derby-driver-url></derby-driver-url>\n"
                                  + "    <mongo-driver-url></mongo-driver-url>\n"
                                  + "    <mysql-driver-url></mysql-driver-url>\n"
                                  + "    <pgsql-driver-url></pgsql-driver-url>\n"
                                  + "    <config-authorized-users></config-authorized-users>\n"
                                  + "    <config-authorized-groups></config-authorized-groups>\n"
                                  + "    <row-limit-type>"+btoa("Soft")+"</row-limit-type>\n"
                                  + "    <row-limit-value></row-limit-value>\n"
                                  + "    <timeout-limit-type>"+btoa("Soft")+"</timeout-limit-type>\n"
                                  + "    <timeout-limit-value></timeout-limit-value>\n"
                                  + "    <notification-email></notification-email>\n"
                                  + "    <atlassian-log-level>"+btoa("Info")+"</atlassian-log-level>\n"
                                  + "    <log-email></log-email>\n"
                                  + "    <email-content-format>"+btoa("Plain Text")+"</email-content-format>\n"
                                  + "    <audit-log-db-profile></audit-log-db-profile>\n"
                                  + "    <log-entry-table-name></log-entry-table-name>\n"
                                  + "    <log-entry-life-time></log-entry-life-time>\n"
                                  + "    <max-cache-lifetime-in-days>"+btoa("7")+"</max-cache-lifetime-in-days>\n"
                                  + "</plugin-configuration>";

                // If XML doesn't validate, just give an empty document
                try {
                    $xml = AJS.$(AJS.$.parseXML(decodeURIComponent(pluginConfiguration.xml)));
                } catch(err) {
                    if( pluginConfiguration.xml != null){
                       console.error("Failed to parse XML: ", err);
                    }
                    $xml = AJS.$(AJS.$.parseXML(defaultSchema));
                }

                // Check for breaking changes in the schema
                if ($xml.find("schema-version").length != 0) {
                    var xmlVersion = $xml.find("schema-version").html();
                    var xmlMajorVer = xmlVersion.split(".")[0];
                    if (xmlMajorVer != schemaVersionArr[0]) {
                        $xml = AJS.$(AJS.$.parseXML(defaultSchema));
                    }
                }

                // Get url values and default them to below constants if they aren't set
                var jTdsDriverUrl = getValueFromXml($xml, "jtds-driver-url");
                var microsoftDriverUrl = getValueFromXml($xml, "microsoft-driver-url");
                var derbyDriverUrl = getValueFromXml($xml, "derby-driver-url");
                var mongoDriverUrl = getValueFromXml($xml, "mongo-driver-url");
                var mysqlDriverUrl = getValueFromXml($xml, "mysql-driver-url");
                var pgsqlDriverUrl = getValueFromXml($xml, "pgsql-driver-url");

                if (!jTdsDriverUrl) {
                    jTdsDriverUrl = "http://central.maven.org/maven2/net/sourceforge/jtds/jtds/1.3.1/jtds-1.3.1.jar";
                }
                if (!microsoftDriverUrl) {
                    microsoftDriverUrl = "http://central.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/6.1.0.jre8/mssql-jdbc-6.1.0.jre8.jar";
                }
                if (!derbyDriverUrl) {
                    derbyDriverUrl = "http://central.maven.org/maven2/org/apache/derby/derby/10.12.1.1/derby-10.12.1.1.jar";
                }
                if (!mongoDriverUrl) {
                    mongoDriverUrl = "http://central.maven.org/maven2/org/mongodb/mongodb-driver/3.4.2/mongodb-driver-3.4.2.jar";
                }
                if (!mysqlDriverUrl) {
                    mysqlDriverUrl = "http://central.maven.org/maven2/mysql/mysql-connector-java/6.0.6/mysql-connector-java-6.0.6.jar";
                }
                if (!pgsqlDriverUrl) {
                    pgsqlDriverUrl = "https://jdbc.postgresql.org/download/postgresql-42.1.1.jar";
                }

                $("#jdbc-driver-directory").val(getValueFromXml($xml,"jdbc-driver-directory"));
                $("#jtds-driver-url").val(jTdsDriverUrl);
                $("#microsoft-driver-url").val(microsoftDriverUrl);
                $("#derby-driver-url").val(derbyDriverUrl);
                $("#mongo-driver-url").val(mongoDriverUrl);
                $("#mysql-driver-url").val(mysqlDriverUrl);
                $("#pgsql-driver-url").val(pgsqlDriverUrl);

                // Pulling other settings from the xml
                var configAuthorizedUsers = getValueFromXml($xml, "config-authorized-users");
                var configAuthorizedGroups = getValueFromXml($xml, "config-authorized-groups");
                var rowLimitType = getValueFromXml($xml, "row-limit-type");
                var rowLimitValue = getValueFromXml($xml, "row-limit-value");
                var timeoutLimitType = getValueFromXml($xml, "timeout-limit-type");
                var timeoutLimitValue = getValueFromXml($xml, "timeout-limit-value");
                var notificationEmail = getValueFromXml($xml, "notification-email");

                var atlassianLogLevel = getValueFromXml($xml, "atlassian-log-level");
                var notifyAll = getValueFromXml($xml, "log-email");
                var emailContentFormat = getValueFromXml($xml, "email-content-format");
                var auditLogDbProfile = getValueFromXml($xml, "audit-log-db-profile");
                var logEntryTableName = getValueFromXml($xml, "log-entry-table-name");
                var logEntryLifeTime = getValueFromXml($xml, "log-entry-life-time");
                var maxCacheLifetimeInDays = getValueFromXml($xml, "max-cache-lifetime-in-days");

                // If we failed to get the fields from the xml, default to empty string;
                if (configAuthorizedUsers == null) {
                    configAuthorizedUsers = "";
                }
                if (configAuthorizedGroups == null) {
                    configAuthorizedGroups = "";
                }
                if (rowLimitType == null) {
                    rowLimitType = "Soft";
                }
                if (rowLimitValue == null) {
                    rowLimitValue = "";
                }
                if (timeoutLimitType == null) {
                    timeoutLimitType = "Soft";
                }
                if (timeoutLimitValue == null) {
                    timeoutLimitValue = "";
                }
                if (notificationEmail == null) {
                    notificationEmail = "";
                }
                if (emailContentFormat == null) {
                    emailContentFormat = "Plain Text";
                }
                if (atlassianLogLevel == null) {
                    atlassianLogLevel = "Info";
                }
                if (maxCacheLifetimeInDays == null) {
                    maxCacheLifetimeInDays = "7";
                }

                // Load access profiles for this page.
                AJS.$("#configuration-permissions").append(Keysight.Database.Admin.Config.Templates.accessProfile({
                    configAuthorizedUsers: configAuthorizedUsers,
                    configAuthorizedGroups: configAuthorizedGroups
                }));

                AJS.$("#row-limit-type").val( rowLimitType );
                AJS.$("#row-limit-value").val( rowLimitValue );
                AJS.$("#timeout-limit-type").val( timeoutLimitType );
                AJS.$("#timeout-limit-value").val( timeoutLimitValue );
                AJS.$("#notification-email").val( notificationEmail );
                AJS.$("#atlassian-log-level").val( atlassianLogLevel );
                AJS.$("#log-email").val(notifyAll);
                AJS.$("#email-content-format").val( emailContentFormat );
                AJS.$("#audit-log-db-profile").val( auditLogDbProfile );
                AJS.$("#log-entry-table-name").val( logEntryTableName );
                AJS.$("#log-entry-life-time").val( logEntryLifeTime );
                AJS.$("#max-cache-lifetime-in-days").val( maxCacheLifetimeInDays );

                AJS.$("#save-config-auth").click(function(e) {
                    e.preventDefault();
                    databaseConfigHelper.saveConfigAuth();
                });

                AJS.$("#save-performance-security-control-settings").click(function(e) {
                    e.preventDefault();
                    databaseConfigHelper.saveConfigAuth();
                });

                AJS.$("#save-cache-settings").click(function(e) {
                    e.preventDefault();
                    databaseConfigHelper.saveConfigAuth();
                });

                AJS.$("#save-audit-log-settings").click(function(e) {
                    e.preventDefault();
                    databaseConfigHelper.saveConfigAuth();
                });

                AJS.$("#save-custom-cdn").click(function(e) {
                    e.preventDefault();
                    databaseConfigHelper.saveConfigAuth();
                });

            } catch( exception ){
                failedToParseSavedData = true;
                this.disableSaveAsPriorSavedDataCannotBeLoaded();
            }
        }).fail(function(self, status, error) {
            var loadFlag = AJS.flag({
                type: 'error',
                title: 'Failed to fetch configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    function getValueFromXml( $xml, key ){
       var value;
       if( $xml.find(key).length > 0 ){
            value = atob($xml.find(key).html());
       }
       return value;
    }

    function saveConfig() {
        function escapeXml(unsafe) {
            return unsafe.replace(/[<>&'"]/g, function (c) {
                switch (c) {
                    case '<': return '&lt;';
                    case '>': return '&gt;';
                    case '&': return '&amp;';
                    case '\'': return '&apos;';
                    case '"': return '&quot;';
                }
            });
        }

        if( failedToParseSavedData ){
           return true;
        }

        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
               '<schema-version>'+schemaVersionString+'</schema-version>' + "\n" +
            '   <jdbc-driver-directory>' + btoa($("#jdbc-driver-directory").val()) + "</jdbc-driver-directory>\n" +
            '   <jtds-driver-url>'+btoa($("#jtds-driver-url").val())+'</jtds-driver-url>\n'+
            '   <microsoft-driver-url>'+btoa($("#microsoft-driver-url").val())+'</microsoft-driver-url>\n'+
            '   <derby-driver-url>'+btoa($("#derby-driver-url").val())+'</derby-driver-url>\n'+
            '   <mongo-driver-url>'+btoa($("#mongo-driver-url").val())+'</mongo-driver-url>\n'+
            '   <mysql-driver-url>'+btoa($("#mysql-driver-url").val())+'</mysql-driver-url>\n'+
            '   <pgsql-driver-url>'+btoa($("#pgsql-driver-url").val())+'</pgsql-driver-url>\n'+
            '   <config-authorized-users>' + btoa($("#config-authorized-users").val()) + "</config-authorized-users>\n" +
            '   <config-authorized-groups>' + btoa($("#config-authorized-groups").val()) + "</config-authorized-groups>\n" +
            '   <row-limit-type>' + btoa($("#row-limit-type").val()) + "</row-limit-type>\n" +
            '   <row-limit-value>' + btoa($("#row-limit-value").val()) + "</row-limit-value>\n" +
            '   <timeout-limit-type>' + btoa($("#timeout-limit-type").val()) + "</timeout-limit-type>\n" +
            '   <timeout-limit-value>' + btoa($("#timeout-limit-value").val()) + "</timeout-limit-value>\n" +
            '   <notification-email>' + btoa($("#notification-email").val()) + "</notification-email>\n" +
            '   <atlassian-log-level>' + btoa($("#atlassian-log-level").val()) + "</atlassian-log-level>\n" +
            '   <log-email>' + btoa($("#log-email").val()) + "</log-email>\n" +
            '   <email-content-format>' + btoa($("#email-content-format").val()) + "</email-content-format>\n" +
            '   <audit-log-db-profile>' + btoa($("#audit-log-db-profile").val()) + "</audit-log-db-profile>\n" +
            '   <log-entry-table-name>' + btoa($("#log-entry-table-name").val()) + "</log-entry-table-name>\n" +
            '   <log-entry-life-time>' + btoa($("#log-entry-life-time").val()) + "</log-entry-life-time>\n" +
            '   <max-cache-lifetime-in-days>' + btoa($("#max-cache-lifetime-in-days").val()) + "</max-cache-lifetime-in-days>\n" +
            '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            AJS.$(AJS.$.parseXML(xmlString))
        } catch(err) {
            console.error("Malformed XML!: ", err);
            return;
        }

        AJS.$.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
            processData: false
        }).done(function() {
            var saveSuccessFlag = AJS.flag({
                type: 'success',
                title: 'Success!',
                body: 'Plugin configuration was saved successfully.',
                close: 'auto'
            });
        }).fail(function(self, status, error) {
            var saveFailFlag = AJS.flag({
                type: 'error',
                title: 'Failed to save configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    methods['disableSaveAsPriorSavedDataCannotBeLoaded'] = function(){
        alert( "We do apologize.  Something went wrong parsing the saved configurations. "
             + "The ability to save had been turned off to prevent destroying "
             + "the already saved data. You might want to try Chrome as some versions "
             + "of IE have this problem.  We have spent quite a bit of time to get IE "
             + "to work correctly and for now have given up.");

        AJS.$("#upload-config-files").unbind("click");
        AJS.$("#upload-config-files").click(function(e) {
            e.preventDefault();
        });
        AJS.$("#save-cache-settings").unbind("click");
        AJS.$("#save-cache-settings").click(function(e) {
            e.preventDefault();
        });
        AJS.$("#save-audit-log-settings").unbind("click");
        AJS.$("#save-audit-log-settings").click(function(e) {
            e.preventDefault();
        });
        AJS.$("#save-jdbc-driver-directory").unbind("click");
        AJS.$("#save-jdbc-driver-directory").click(function(e) {
            e.preventDefault();
        });
        AJS.$("#save-custom-cdn").unbind("click");
        AJS.$("#save-custom-cdn").click(function(e) {
            e.preventDefault();
        });
        AJS.$("#save-performance-security-control-settings").unbind("click");
        AJS.$("#save-performance-security-control-settings").click(function(e) {
            e.preventDefault();
        });
    }

    return methods;
})(AJS.$ || jQuery);

AJS.toInit(function() {

    if( databaseConfigHelper.canParseSavedResults() ){
       AJS.$("#save-jdbc-driver-directory").click(function(e) {
          e.preventDefault();
           databaseConfigHelper.saveJdbcDriverDirectory();
       });

       databaseConfigHelper.loadConfig();
    } else {
       databaseConfigHelper.disableSaveAsPriorSavedDataCannotBeLoaded();
    }
});