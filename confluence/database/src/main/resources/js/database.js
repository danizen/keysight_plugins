// The pattern below is a 'module' pattern based upon iife (immediately invoked function expressions) closures.
// see: http://benalman.com/news/2010/11/immediately-invoked-function-expression/ for a nice discussion of the pattern
// The value of this pattern is to help us keep our variables to ourselves.
var databaseHelp = (function( $ ){
	
   // module variables
   var methods     = new Object();
   var pluginId    = "database";
   var restVersion = "1.0";

   // module methods
   methods[ 'showDatabaseQueryHelp' ] = function( e ){
      macroHelpDocumentation.getMacroHelp( e, pluginId, restVersion, "database-query" );
   }
   methods[ 'showMacroResultCacheHelp' ] = function( e ){
      macroHelpDocumentation.getMacroHelp( e, pluginId, restVersion, "macro-result-cache" );
   }

   // return the object with the methods
   return methods;

// end closure
})( AJS.$ || jQuery );


function forceDatabaseQueryCacheRefresh( macroId )
{
    var apiUrl = AJS.Data.get("base-url") + "/rest/database/1.0/cache/databaseQuery/clear/";
    forceCacheRefresh( apiUrl, macroId );
}

function forceMacroResultsCacheCacheRefresh( macroId )
{
    var apiUrl = AJS.Data.get("base-url") + "/rest/database/1.0/cache/macroResultsCache/clear/";
    forceCacheRefresh( apiUrl, macroId );
}

function forceCacheRefresh( apiUrl, macroId )
{
    AJS.$.ajax({
        async: true,
        url: apiUrl + macroId,
        dataType: "json",
        error: function(xhr, textStatus, errorThrown) {
            console.error("Couldn't clear the cache for macro-id: " + macroId, errorThrown);
        },
        success: function(data) {
            location.reload();
        }
    });
}
