package com.keysight.include.content.helpers;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.MacroDefinitionMarshallingStrategy;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.Permission;

import com.keysight.include.content.helpers.SharedBlockIncludeStack;
import com.keysight.include.content.helpers.IncludeContentHelper;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class PreviewBuilder
{
    private static final Logger log = LoggerFactory.getLogger(PreviewBuilder.class);

    private static final String SHARED_BLOCK_KEY = "shared-block-key";
    private static final String SUPPRESS_ERRORS  = "suppress-errors";
    private static final String ERROR            = "Error: ";
    private static final String ALREADY_INCLUDED = "The page is already included";
    private static final String ALLOW_ANONYMOUS_ACCESS = "allowAnonymousAccess";

    private final PermissionManager permissionManager;
    private final Renderer renderer;
    private final XhtmlContent xhtmlUtils;

    @Autowired
    public PreviewBuilder( PermissionManager permissionManager,
                           Renderer renderer,
                           XhtmlContent xhtmlUtils
    ){
        this.permissionManager  = permissionManager;
        this.renderer     = renderer;
        this.xhtmlUtils   = xhtmlUtils;
    }

    public String getContentPreview(ContentEntityObject page,
                                    Map<String, String[]> parameterMap,
                                    ConversionContext conversionContext,
                                    String contentPreviewMacro)
    {

        String blockId = page.getTitle();
        if( parameterMap.containsKey( SHARED_BLOCK_KEY ) ){
            blockId = blockId + ":" + parameterMap.get( SHARED_BLOCK_KEY )[0];
        }

        if (SharedBlockIncludeStack.contains(blockId))
            return RenderUtils.blockError( ERROR, ALREADY_INCLUDED );

        SharedBlockIncludeStack.push(blockId);
        try {
            String content = getContentPreviewHtml( parameterMap, page, conversionContext, contentPreviewMacro );
            DefaultConversionContext context = new DefaultConversionContext(new PageContext(page, conversionContext.getPageContext()));
            return renderer.render(content, context);
        } catch (Exception e){
            log.warn( e.toString() );
        } finally {
            SharedBlockIncludeStack.pop();
        }
        return RenderUtils.blockError( ERROR, "Something went wrong" );
    }

    private String getContentPreviewHtml( Map<String,String[]> parameterMap, ContentEntityObject page, ConversionContext context, String previewContentMacro ) throws MacroExecutionException
    {
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
        String html = "";
        try{
            xhtmlUtils.handleMacroDefinitions(page.getBodyContent().getBody(), context, new MacroDefinitionHandler(){
                        @Override
                        public void handle(MacroDefinition macroDefinition){
                            macros.add(macroDefinition);
                        }
                    },
                    MacroDefinitionMarshallingStrategy.MARSHALL_MACRO);
        } catch (XhtmlException e) {
            throw new MacroExecutionException(e);
        }

        if (!macros.isEmpty()) {
            for( MacroDefinition macroDefinition : macros ){
                if( parameterMap.containsKey(SHARED_BLOCK_KEY) ){
                    if( macroDefinition.getName().equals( previewContentMacro )
                            && parameterMap.get(SHARED_BLOCK_KEY)[0].equals( macroDefinition.getParameter(SHARED_BLOCK_KEY) ) ){
                        if( macroDefinition.getParameter( ALLOW_ANONYMOUS_ACCESS ) != null || permissionManager.hasPermission(currentUser, Permission.VIEW, page) ){
                            html = macroDefinition.getBodyText();
                        } else {
                            if( parameterMap.containsKey(SUPPRESS_ERRORS)){
                                html = "";
                            } else {
                                html = RenderUtils.blockError(ERROR + " You do not have permissions to view this content.", "" );
                            }
                        }
                        break;
                    }
                } else {
                    if( macroDefinition.getName().equals(previewContentMacro) ){
                        if( macroDefinition.getParameter(ALLOW_ANONYMOUS_ACCESS) != null || permissionManager.hasPermission(currentUser, Permission.VIEW, page) ){
                            html = macroDefinition.getBodyText();
                        } else {
                            if( parameterMap.containsKey(SUPPRESS_ERRORS) ){
                                html = "";
                            } else {
                                html = RenderUtils.blockError(ERROR + " You do not have permissions to view this content.", "" );
                            }
                        }
                        break;
                    }
                }
            }
        }

        return html;
    }
}
