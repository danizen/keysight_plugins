(function ($) {


    $(document).ready(function() {
       bindLikeThisControl();
    });
})(AJS.$ || jQuery);

function bindLikeThisControl(){
   // form the URL
   var url = AJS.contextPath() + "/rest/include-content/1.0/faq/toggle-like";

   AJS.$(".keysight-faq-like-control").unbind( "click" );
   AJS.$(".keysight-faq-like-control").click(function(e) {
      e.preventDefault();

      // request the config information from the server
      $.ajax({
          url: url,
          type: "GET",
          dataType: "json",
          contentType: "application/x-www-form-urlencoded",
          data: {"PAGE_ID":$(e.target).parent("a").attr("pageId")}
      }).done(function(data) { // when the configuration is returned...
         if( data.userLikePreference === "liked" ){
             $(e.target).html( "(Unlike)" );
             $(e.target).closest("tr.keysight-faq-list-row").find(".keysight-faq-like-description").html( "You like this." );
         } else {
             $(e.target).html( "Like" );
             $(e.target).closest("div").find(".keysight-faq-like-description").html( "" );
         }
          $(e.target).closest("tr.keysight-faq-list-row").find("td.faq-like-count").html( data.likeCount );
      }).fail(function(self,status,error){
          console.error( error );
      });
   });
   return;
}
