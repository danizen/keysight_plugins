<h3>Introduction</h3>

<p>The Include Shared Block with Replacement Macro uses regular
expression substitution rules defined in the body of the
macro to display the contents of a Shared Block defined in 
another confluence page with simple string substitution.</p>

<h3>Usage</h3>

<h4>Identifying the Shared Block</h4>

<p>In the macro parameters, the page on which the shared
block is defined needs to be specified.  Optionally, the
key identifying the shared block may be provided.  The key
is a string that needs to match in both the Shared Block
and the Include Shared Block before the contents may be
pulled in. If no key is specified, the first shared block
on the page will be pulled in</p>

<h4>Defining String Replacement Rules</h4>

<p>A substitution rules takes the form of</p>

<p><pre>/original text/new text/</pre></p>

<p>Every line in the body of the macro defines a new rule
that will be executed in order (top to bottom).  The default
delimiter is &quot;/&quot, however, that can be changed
to any string using the &quot;Delimiter&quot; parameter in the
macro definition.</p>

<h3>Warning to the User</h3>

<p>The substitution is performed on the final html and does not
understand the difference between the page content and the html
formatting, thus it can transform the underlying xml into an
invalid state causing the macro to be un-renderable.</p>

<h3>Parameters</h3>

<p><strong>Page</strong>:The name of the page containing the shared block
to be displayed.</p>

<p><strong>Shared Block Key</strong>:If a <b>Shared Block</b> on the target
page has this string set in it's macro properties, that block will be
inserted. If no key is set, the first shared block on the page will be included.
If a key is set but no block matches, an error will be displayed.</p>

<p><strong>Delimiter</strong>:The character to use to mark the start, middle
and end of the substitution expression.  For example, if / is the 
delimiter a valid substitution expression is <b>/this/that/</b>.  If the character
&quot;!&quot; is the delimiter, the same expression now looks like <b>!this!that!</b>.</p>

<p><strong>Suppress Permission Errors</strong>:If checked and there is a
permissions problem when rendering the macro, no error message will be
displayed.  For example, if the shared block is on a page the current user does
not have permissions to see, then they will not see an error informing them
that they don't have permissions to see the content.</p>
