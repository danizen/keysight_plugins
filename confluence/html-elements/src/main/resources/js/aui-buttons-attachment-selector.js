(function($) {
  
  var macroName = "download-attachment-button";
  var attachmentFileTypes;
 
  AJS.MacroBrowser.activateSmartFieldsAttachmentsOnPage(macroName, attachmentFileTypes);
})(AJS.$);
