package com.keysight.test.ecosystem.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.renderer.v2.RenderUtils;

import java.util.Map;

public class EtmsStationStatus implements Macro
{
   public final static String HOSTNAMES_KEY = "hostnames";
   public final static String SHOW_FIELDS_KEY = "show-fields";
   public final static String HIDE_NAVIGATION_BOX_KEY = "hide-navigation-box";
   public final static String STYLE_KEY = "navigation-box-style";
   public final static String WIDTH_KEY = "navigation-box-width";
   public final static String HIDE_NAVIGATION_BOX_HEADER = "hide-navigation-box-header";
   public final static String HEADER_STYLE_KEY = "headerStyle";
   public final static String REFRESH_INTERVAL_KEY = "refresh-interval";
   public final static String BODY_STYLE_KEY = "bodyStyle";
   public final static String BYPASS_PROXY_KEY = "bypass-proxy";

   protected final VelocityHelperService velocityHelperService;
   protected final SettingsManager settingsManager;

   public EtmsStationStatus( SettingsManager settingsManager,
		                     VelocityHelperService velocityHelperService)
   {
      this.settingsManager = settingsManager;
      this.velocityHelperService = velocityHelperService;
   }

   @Override
   public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
   {
      String template = "/com/keysight/test-ecosystem/templates/etms-station-status.vm";
      Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
      String baseUrl   = settingsManager.getGlobalSettings().getBaseUrl();

      if( parameters.containsKey(HOSTNAMES_KEY)){
         String[] hostnames = parameters.get(HOSTNAMES_KEY).split("\n");
         velocityContext.put( HOSTNAMES_KEY, hostnames );
      } else {
         return RenderUtils.blockError("Error", "No hostnames provided" );
      }

      if( parameters.containsKey(BYPASS_PROXY_KEY)) {
         velocityContext.put("bypassProxy", "true");
      } else {
         velocityContext.put("bypassProxy", "false");
      }

      if( !parameters.containsKey(HIDE_NAVIGATION_BOX_KEY)) {
         velocityContext.put("addNavigationBox", "true");
      }

      if( parameters.containsKey(REFRESH_INTERVAL_KEY)) {
         velocityContext.put("refreshInterval", parameters.get(REFRESH_INTERVAL_KEY));
      } else {
         velocityContext.put("refreshInterval", "-1" );
      }

      if( !parameters.containsKey(HIDE_NAVIGATION_BOX_HEADER)) {
         velocityContext.put("showHeader", "true" );
      }

      if( parameters.containsKey(SHOW_FIELDS_KEY)) {
         String[] fields = parameters.get( SHOW_FIELDS_KEY ).split("\n");
         velocityContext.put("showFields", String.join( ",", fields ));
      } else {
         velocityContext.put("showFields", "*" );
      }

      if( parameters.containsKey(WIDTH_KEY)) {
         velocityContext.put("navigationBoxWidth", parameters.get(WIDTH_KEY) );
      }

      if( parameters.containsKey( STYLE_KEY ) ) {
         if (parameters.get(STYLE_KEY).equals("Keysight Dark Red")) {
            velocityContext.put(HEADER_STYLE_KEY, "etms-station-status etms-station-status-header etms-station-status-top etms-station-status-keysight-dark-red etms-station-status-keysight-dark-red-header");
            velocityContext.put(BODY_STYLE_KEY, "etms-station-status etms-station-status-body   etms-station-status-keysight-dark-red etms-station-status-keysight-dark-red-body");
         } else if (parameters.get(STYLE_KEY).equals("Keysight Gray")) {
            velocityContext.put(HEADER_STYLE_KEY, "etms-station-status etms-station-status-header etms-station-status-top etms-station-status-keysight-gray etms-station-status-keysight-gray-header");
            velocityContext.put(BODY_STYLE_KEY, "etms-station-status etms-station-status-body   etms-station-status-keysight-gray keysight-gray-body");
         } else {  // Keysight Dark Gray
            velocityContext.put(HEADER_STYLE_KEY, "etms-station-status etms-station-status-header etms-station-status-top etms-station-status-keysight-dark-gray etms-station-status-keysight-dark-gray-header");
            velocityContext.put(BODY_STYLE_KEY, "etms-station-status etms-station-status-body   etms-station-status-keysight-dark-gray etms-station-status-keysight-dark-gray-body");
         }
      } else { // Default to Keysight Dark Gray
         velocityContext.put(HEADER_STYLE_KEY, "etms-station-status etms-station-status-header etms-station-status-top etms-station-status-keysight-dark-gray etms-station-status-keysight-dark-gray-header");
         velocityContext.put(BODY_STYLE_KEY, "etms-station-status etms-station-status-body   etms-station-status-keysight-dark-gray etms-station-status-keysight-dark-gray-body");
      }

      return velocityHelperService.getRenderedTemplate(template, velocityContext);
   }

   @Override
   public BodyType getBodyType()
   {
      return BodyType.NONE;
   }

   @Override
   public OutputType getOutputType()
   {
      return OutputType.BLOCK;
   }
}
