package com.keysight.header.menus.helpers;

import com.atlassian.confluence.plugin.descriptor.web.conditions.BaseConfluenceCondition;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.plugin.PluginParseException;
import org.apache.commons.lang.StringUtils;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddPopupMenuCondition extends BaseConfluenceCondition
{
    private static final Logger log = LoggerFactory.getLogger(AddPopupMenuCondition.class);
    private PluginConfigManager pluginConfigManager;
    protected String menuLetter = "A";
    private static String MENU_LETTER = "menu-letter";

    protected String getMenuLetter(){
        return this.menuLetter;
    }

    protected void setMenuLetter( String menuLetter ){
        this.menuLetter = menuLetter;
    }

    @Override
    public void init(Map<String,String> params) throws PluginParseException
    {
        if( params.containsKey(MENU_LETTER)){
            this.setMenuLetter(params.get(MENU_LETTER));
        }
    }

    @Override
    public boolean shouldDisplay(WebInterfaceContext context)
    {
        boolean bFlag = false;
        Menu menu = pluginConfigManager.getMenu( this.getMenuLetter() );
        if( StringUtils.isNotBlank( menu.label ) && menu.hasPopupMenu() ){
            return true;
        }

        return bFlag;
    }

    public void setPluginConfigManager(PluginConfigManager pluginConfigManager) {
        this.pluginConfigManager = pluginConfigManager;
    }
}
