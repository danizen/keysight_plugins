package com.keysight.header.menus.helpers;

import org.apache.commons.lang.StringUtils;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class PopupMenuItem {
    public String label;
    public String url;
    public String type;

    private static String LINK = "link";
    private static String SEPARATOR = "separator";
    private static String HEADING = "heading";

    public PopupMenuItem() {
    }

    public PopupMenuItem(String inputString) {

        inputString = inputString.trim();
        if( inputString.matches( "^-+$")) {
            this.setType(SEPARATOR);
        } else if( inputString.matches( ".+,.+")) {
            this.setType(LINK);
            List<String> elements = Arrays.asList(inputString.split(",", 2));
            this.setLabel(elements.get(0));
            this.setUrl(elements.get(1));
        } else if( StringUtils.isNotBlank( inputString ) ) {
            this.setType(HEADING);
            this.setLabel(inputString);
        }
    }

    public PopupMenuItem(String label, String url) {
        this.setLabel(label);
        this.setUrl(url);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
