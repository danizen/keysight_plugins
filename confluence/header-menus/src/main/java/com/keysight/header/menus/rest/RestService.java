package com.keysight.header.menus.rest;

import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.keysight.header.menus.helpers.PluginConfigManager;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
public class RestService {
    private static final Logger log = LoggerFactory.getLogger(RestService.class);

    private final PluginConfigManager pluginConfigManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final SettingsManager settingsManager;
    private final TransactionTemplate transactionTemplate;
    private final UserManager userManager;
    private final UserAccessor userAccessor;
    private final VelocityHelperService velocityHelperService;

    public RestService(PluginConfigManager pluginConfigManager,
                       PluginSettingsFactory pluginSettingsFactory,
                       SettingsManager settingsManager,
                       TransactionTemplate transactionTemplate,
                       UserManager userManger,
                       UserAccessor userAccessor,
                       VelocityHelperService velocityHelperService
    ) {
        this.pluginConfigManager = pluginConfigManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.settingsManager = settingsManager;
        this.transactionTemplate = transactionTemplate;
        this.userAccessor = userAccessor;
        this.userManager = userManger;
        this.velocityHelperService = velocityHelperService;
    }

    // Documentation routes

    /*
    @GET
    @Path("help/database-query")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response databaseQueryHelp() {
        String title = "Database Query";
        String bodyTemplate = "/com/keysight/database/templates/database-query-help.vm";

        return getMacroHelp(title, bodyTemplate);
    }
    */

    // Helpers
    private Response getMacroHelp(String title, String bodyTemplate) {
        StringBuilder html = new StringBuilder();
        String headerTemplate = "/com/keysight/header-menus/templates/help-header.vm";
        String footerTemplate = "/com/keysight/header-menus/templates/help-footer.vm";
        String fossTemplate = "/com/keysight/header-menus/templates/foss.vm";

        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
        velocityContext.put( "title", title );
        velocityContext.put( "baseUrl", settingsManager.getGlobalSettings().getBaseUrl() );

        html.append( velocityHelperService.getRenderedTemplate( headerTemplate, velocityContext ) );
        html.append( velocityHelperService.getRenderedTemplate( bodyTemplate,   velocityContext ) );
        html.append( velocityHelperService.getRenderedTemplate( fossTemplate,   velocityContext ) );
        html.append( velocityHelperService.getRenderedTemplate( footerTemplate, velocityContext ) );

        return Response.ok( new RestResponse( html.toString() ) ).build();
    }
}
