package com.keysight.header.menus.helpers;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;

import org.apache.commons.lang.StringUtils;
import java.util.Map;
import java.util.List;

public class NavbarLinkContextProvider implements ContextProvider {
    private final PluginConfigManager pluginConfigManager;
    private static String URL = "url";
    private static String LABEL = "label";
    protected String menuLetter = "A";
    private static String MENU_LETTER = "menu-letter";

    protected String getMenuLetter(){
        return this.menuLetter;
    }

    protected void setMenuLetter( String menuLetter ){
        this.menuLetter = menuLetter;
    }

    public void init(Map<String,String> params) throws PluginParseException {
        if (params.containsKey(MENU_LETTER)) {
            this.setMenuLetter(params.get(MENU_LETTER));
        }
    }

    public NavbarLinkContextProvider( PluginConfigManager pluginConfigManager ){
        this.pluginConfigManager = pluginConfigManager;
    }

    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Menu menu = pluginConfigManager.getMenu( this.getMenuLetter() );
        if( StringUtils.isNotBlank(menu.getLabel()) ){
            context.put( LABEL, menu.getLabel() );
            context.put( URL, menu.getUrl() );
        } else {
            context.put( LABEL, "No Label" );
            context.put( URL, "" );
        }
        return context;
    }
}
