// The pattern below is a 'module' pattern based upon iife (immediately invoked function expressions) closures.
// see: http://benalman.com/news/2010/11/immediately-invoked-function-expression/ for a nice discussion of the pattern
// The value of this pattern is to help us keep our variables to ourselves.
var headerMenusHelp = (function( $ ){
	
   // module variables
   var methods     = new Object();
   var pluginId    = "header-menus";
   var restVersion = "1.0";

   // module methods
   // commented out as this plugin presently has no macros.
   //methods[ 'showDatabaseQueryHelp' ] = function( e ){
      //macroHelpDocumentation.getMacroHelp( e, pluginId, restVersion, "database-query" );
   //}

   // return the object with the methods
   return methods;

// end closure
})( AJS.$ || jQuery );

function headerMenusAddDropdownAttributes(selectedElement, name){
    selectedElement.attr("aria-haspopup", "true");
    selectedElement.attr("aria-owns", name + "-menu-link-content")
    selectedElement.addClass("aui-nav-link");
    selectedElement.addClass("aui-dropdown2-trigger");
}

/* Not using a closure to generate this html as there
does not seem to be the ability to create a local variable
and change it to keep track of opening and closing the
section.  i.e. I can't create inSection using closures */
function generateDropDownMenu(menuLetter, popupLinks)
{
   var html = [];
   var inSection = false;

   html.push( "<nav id=\"menu-"+menuLetter+"-menu-link-content\" class=\"aui-dropdown2 aui-style-default\" aria-hidden=\"true\">\n" );
   for( var i = 0; i < popupLinks.length; i++ ){
      if( popupLinks[i].type == "heading"){
         if( inSection ){
            html.push( "      </ul>\n" );
            html.push( "   </div>\n" );
         }
         html.push( "   <div class=\"aui-dropdown2-section\">\n" );
         html.push( "      <div class=\"aui-dropdown2-heading\">\n" );
         html.push( "         <strong>"+popupLinks[i].label+"</strong>\n" );
         html.push( "      </div>\n" );
         html.push( "      <ul>\n" );
         inSection = true;
      } else if( popupLinks[i].type == "separator"){
         if( inSection ){
            html.push( "      </ul>\n" );
            html.push( "   </div>\n" );
         }
         inSection = false;
      } else if( popupLinks[i].type == "link"){
         if( !inSection ){
            html.push( "   <div class=\"aui-dropdown2-section\">\n" );
            html.push( "      <ul>\n" );
            inSection = true;
         }
         html.push( "         <li><a href=\""+popupLinks[i].url+"\">"+popupLinks[i].label+"</a></li>\n" );
      }
   }
   html.push( "      </ul>\n" );
   html.push( "   </div>\n" );
   html.push( "</nav>\n" );

   AJS.$('body').append( html.join("") );
   headerMenusAddDropdownAttributes( AJS.$("a#menu-"+menuLetter), "menu-"+menuLetter);
}

