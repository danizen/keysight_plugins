package com.keysight.keysight.theme.helpers;

import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Iterator;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdminConfigUI extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(AdminConfigUI.class);

    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final TemplateRenderer templateRenderer;
    private final PluginConfigManager pluginConfigManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;
    private final SettingsManager settingsManager;

    private boolean tryToSaveConfigXml;
    private boolean savedConfigXml;

    public AdminConfigUI(LoginUriProvider loginUriProvider,
                         PluginConfigManager pluginConfigManager,
                         PluginSettingsFactory pluginSettingsFactory,
                         SettingsManager settingsManager,
                         TemplateRenderer templateRenderer,
                         TransactionTemplate transactionTemplate,
                         UserManager userManager) {
        this.loginUriProvider = loginUriProvider;
        this.pluginConfigManager = pluginConfigManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.settingsManager = settingsManager;
        this.templateRenderer = templateRenderer;
        this.transactionTemplate = transactionTemplate;
        this.userManager = userManager;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        tryToSaveConfigXml = false;
        renderIt(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if( isMultipart )
        {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            try {
                List fileItems = upload.parseRequest(request);
                Iterator fileIterator = fileItems.iterator();
                tryToSaveConfigXml = false;
                while ( fileIterator.hasNext () ) {
                    FileItem fileItem = (FileItem)fileIterator.next();
                    if ( !fileItem.isFormField () ) {
                        String fieldName = fileItem.getFieldName();
                        String fileContents = fileItem.getString();

                        if( fieldName.equals("upload-config") && !StringUtils.isEmpty( fileContents ))
                        {
                            savedConfigXml = pluginConfigManager.setConfigXml(fileContents);
                            tryToSaveConfigXml = true;
                        }
                    }
                }
            } catch(Exception exception) {
                log.warn( "Failed to upload configuration file: " + exception.getMessage() );
            }
        }

        renderIt(request, response);
    }

    public void renderIt(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (!isAuthorized(request)) {
            redirectToLogin(request, response);
            return;
        }

        Map<String, Object> velocityContext = new HashMap<>();
        velocityContext.put("relativeHome", new File(".").getCanonicalPath());

        if( tryToSaveConfigXml ){
            velocityContext.put("tryToSaveConfigXml", "true" );
            if( savedConfigXml ) {
                velocityContext.put("savedConfigXml", "true");
            }
        }

        velocityContext.put( "baseUrl", settingsManager.getGlobalSettings().getBaseUrl() );

        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render("/com/keysight/keysight-theme/templates/admin-ui.vm", velocityContext, response.getWriter());
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private boolean isAuthorized(HttpServletRequest request) {
        UserKey userKey = userManager.getRemoteUserKey(request);

        // Check basic conditions.
        if (userKey == null) {
            return false;
        } else if (userManager.isSystemAdmin(userKey)) {
            return true;
        }

        return false;
    }
}
