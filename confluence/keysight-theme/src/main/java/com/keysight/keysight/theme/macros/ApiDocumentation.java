package com.keysight.keysight.theme.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.renderer.v2.RenderUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.Authenticator;
import java.net.URLDecoder;
import java.net.PasswordAuthentication;
import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.net.URLEncoder;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import com.keysight.keysight.theme.helpers.PluginConfigManager;
import com.keysight.keysight.theme.helpers.KeysightThemeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiDocumentation implements Macro
{
   private static final Logger log = LoggerFactory.getLogger(ApiDocumentation.class);
   public final static String APPLICATION   = "application";
   public final static String SHOW_COLLAPSED = "show-collapsed";
   public final static String HIDE_APPLICATION_NAME = "hide-application-name";
   public final static String API_SERVER = KeysightThemeConstants.getDefaultApiDocumentationServer();

   protected final PluginConfigManager pluginConfigManager;
   protected final SettingsManager settingsManager;
   protected final VelocityHelperService velocityHelperService;

   public ApiDocumentation( PluginConfigManager pluginConfigManager,
                            SettingsManager settingsManager,
		                    VelocityHelperService velocityHelperService)
   {
      this.pluginConfigManager = pluginConfigManager;
      this.settingsManager = settingsManager;
      this.velocityHelperService = velocityHelperService;
   }

   @Override
   public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
      String template = "/com/keysight/keysight-theme/templates/api-documentation.vm";
      Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
      String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
      String inputLine;
      String html;
      StringBuilder text = new StringBuilder();
      Map<String, String[]> applicationVersions = new HashMap<String, String[]>();

      String serviceUrl = pluginConfigManager.getApiDocumentationServiceUrl();
      log.warn( "The Service URL in the Macro is: " + serviceUrl );

      try {
         URL serverUrl = new URL(serviceUrl);
         velocityContext.put("apiServer", serverUrl.getHost());
      } catch( Exception exception ){
         velocityContext.put("apiServer", KeysightThemeConstants.getDefaultApiDocumentationServer());
      }

      if( parameters.containsKey( SHOW_COLLAPSED ) ){
         velocityContext.put( "collapsed", "true" );
      }

      if( parameters.containsKey( APPLICATION ) ){
         try{
            String urlEncodedApplication = URLEncoder.encode(parameters.get( APPLICATION ).trim(), StandardCharsets.UTF_8.toString());
            URL url = new URL( serviceUrl + "?GET_APPLICATION_VERSION_TREE=TRUE&text=" + urlEncodedApplication );
            URLConnection browser = url.openConnection();
            BufferedReader reader = new BufferedReader( new InputStreamReader( browser.getInputStream() ) );
            while(( inputLine = reader.readLine()) != null ){
               text.append( inputLine );
            }

            String json = text.toString();
            velocityContext.put( "json", json );

            if( json.matches( "\\s*\\{.*\\}\\s*" ) ){
               Type collectionType = new TypeToken<Map<String,String[]>>(){}.getType();
               Map<String, String[]> applicationTree = new Gson().fromJson( json, collectionType );
               velocityContext.put( "applicationTree", applicationTree );

               if( parameters.containsKey( HIDE_APPLICATION_NAME ) && applicationTree.size() == 1 ){
                  velocityContext.put( "oneApplication", "true" );
               }
         
               if( applicationTree.size() <= 0 ){
                  html = RenderUtils.blockError("Error: No applications found.", "" );
               } else {
                  html = velocityHelperService.getRenderedTemplate(template, velocityContext);
               }
            } else {
               html = RenderUtils.blockError("Error: No applications found.", "" );
            }
         } catch (MalformedURLException e ){
            throw new MacroExecutionException( e );
         } catch (IOException e){
            throw new MacroExecutionException( e );
         }
      }
      else 
      {
         html = RenderUtils.blockError("Error: No applications found.", "" );
      }


      return html;

   }

   @Override
   public BodyType getBodyType()
   {
      return BodyType.NONE;
   }

   @Override
   public OutputType getOutputType()
   {
      return OutputType.BLOCK;
   }
}
