package com.keysight.mathjax.helpers;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.keysight.mathjax.rest.RestAdminConfigService;
import org.apache.commons.validator.routines.UrlValidator;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.lang3.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Helper functions for use throughout the plugin
@Component
public class PluginHelper {
    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;
    private String m_url;
    private String m_inlineMathjaxStartIdentifier;
    private String m_inlineMathjaxEndIdentifier;
    private String m_blockMathjaxStartIdentifier;
    private String m_blockMathjaxEndIdentifier;
    private static String INLINE_MATHJAX_START_IDENTIFIER = "inlineMathjaxStartIdentifier";
    private static String INLINE_MATHJAX_END_IDENTIFIER = "inlineMathjaxEndIdentifier";
    private static String BLOCK_MATHJAX_START_IDENTIFIER = "blockMathjaxStartIdentifier";
    private static String BLOCK_MATHJAX_END_IDENTIFIER = "blockMathjaxEndIdentifier";

    private static final Logger log = LoggerFactory.getLogger(PluginHelper.class);

    @Autowired
    public PluginHelper(PluginSettingsFactory pluginSettingsFactory,
                        TransactionTemplate transactionTemplate ){
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;


        PluginSettings pluginSettings = this.pluginSettingsFactory.createGlobalSettings();
        UrlValidator urlValidator = new UrlValidator();
        String url;
        String inlineMathjaxStartIdentifier;
        String inlineMathjaxEndIdentifier;
        String blockMathjaxStartIdentifier;
        String blockMathjaxEndIdentifier;

        try {
            url = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + ".url");
            if (urlValidator.isValid(url)) {
                this.setUrl( url );
            } else {
                this.setUrl( Constants.DEFAULT_URL );
            }
        } catch (Exception exception) {
            try {
                this.setUrl( Constants.DEFAULT_URL );
            } catch (Exception exception2) {
                log.warn( "Failed to save default url value to the plugin settings: " + exception2.getMessage() );
            }
        }

        try {
            inlineMathjaxStartIdentifier = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + "." + INLINE_MATHJAX_START_IDENTIFIER);
            if ( StringUtils.isNotBlank(inlineMathjaxStartIdentifier)) {
                this.setInlineMathjaxStartIdentifier( inlineMathjaxStartIdentifier );
            } else {
                this.setInlineMathjaxStartIdentifier( Constants.DEFAULT_INLINE_MATHJAX_START_IDENTIFIER );
            }
        } catch (Exception exception) {
            try {
                this.setInlineMathjaxStartIdentifier( Constants.DEFAULT_INLINE_MATHJAX_START_IDENTIFIER );
            } catch (Exception exception2) {
                log.warn( "Failed to save default inline start identifier value to the plugin settings: " + exception2.getMessage() );
            }
        }

        try {
            inlineMathjaxEndIdentifier = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + "." + INLINE_MATHJAX_END_IDENTIFIER);
            if ( StringUtils.isNotBlank(inlineMathjaxEndIdentifier)) {
                this.setInlineMathjaxEndIdentifier( inlineMathjaxEndIdentifier );
            } else {
                this.setInlineMathjaxEndIdentifier( Constants.DEFAULT_INLINE_MATHJAX_END_IDENTIFIER );
            }
        } catch (Exception exception) {
            try {
                this.setInlineMathjaxEndIdentifier( Constants.DEFAULT_INLINE_MATHJAX_END_IDENTIFIER );
            } catch (Exception exception2) {
                log.warn( "Failed to save default inline end identifier value to the plugin settings: " + exception2.getMessage() );
            }
        }

        try {
            blockMathjaxStartIdentifier = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + "." + BLOCK_MATHJAX_START_IDENTIFIER);
            if ( StringUtils.isNotBlank(blockMathjaxStartIdentifier)) {
                this.setBlockMathjaxStartIdentifier( blockMathjaxStartIdentifier );
            } else {
                this.setBlockMathjaxStartIdentifier( Constants.DEFAULT_BLOCK_MATHJAX_START_IDENTIFIER );
            }
        } catch (Exception exception) {
            try {
                this.setBlockMathjaxStartIdentifier( Constants.DEFAULT_BLOCK_MATHJAX_START_IDENTIFIER );
            } catch (Exception exception2) {
                log.warn( "Failed to save default block start identifier value to the plugin settings: " + exception2.getMessage() );
            }
        }

        try {
            blockMathjaxEndIdentifier = (String) pluginSettings.get(RestAdminConfigService.Config.class.getName() + "." + BLOCK_MATHJAX_END_IDENTIFIER);
            if ( StringUtils.isNotBlank(blockMathjaxEndIdentifier)) {
                this.setBlockMathjaxEndIdentifier( blockMathjaxEndIdentifier );
            } else {
                this.setBlockMathjaxEndIdentifier( Constants.DEFAULT_BLOCK_MATHJAX_END_IDENTIFIER );
            }
        } catch (Exception exception) {
            try {
                this.setBlockMathjaxEndIdentifier( Constants.DEFAULT_BLOCK_MATHJAX_END_IDENTIFIER );
            } catch (Exception exception2) {
                log.warn( "Failed to save default block end identifier value to the plugin settings: " + exception2.getMessage() );
            }
        }
    }

    public String getUrl(){
        return this.m_url;
    }
    public void setUrl( final String url ){
        this.m_url = url;
        transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(RestAdminConfigService.Config.class.getName() + ".url", url);
                return null;
            }
        });
    }

    public String getInlineMathjaxStartIdentifier(){
        return this.m_inlineMathjaxStartIdentifier;
    }
    public void setInlineMathjaxStartIdentifier( final String value ){
        if( StringUtils.isNotBlank( value )) {
            this.m_inlineMathjaxStartIdentifier = value;
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                    pluginSettings.put(RestAdminConfigService.Config.class.getName() + "." + INLINE_MATHJAX_START_IDENTIFIER, value);
                    return null;
                }
            });
        }
    }

    public String getInlineMathjaxEndIdentifier(){
        return this.m_inlineMathjaxEndIdentifier;
    }
    public void setInlineMathjaxEndIdentifier( final String value ){
        if( StringUtils.isNotBlank( value )) {
            this.m_inlineMathjaxEndIdentifier = value;
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                    pluginSettings.put(RestAdminConfigService.Config.class.getName() + "." + INLINE_MATHJAX_END_IDENTIFIER, value);
                    return null;
                }
            });
        }
    }

    public String getBlockMathjaxStartIdentifier(){
        return this.m_blockMathjaxStartIdentifier;
    }
    public void setBlockMathjaxStartIdentifier( final String value ){
        if( StringUtils.isNotBlank( value )) {
            this.m_blockMathjaxStartIdentifier = value;
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                    pluginSettings.put(RestAdminConfigService.Config.class.getName() + "." + BLOCK_MATHJAX_START_IDENTIFIER, value);
                    return null;
                }
            });
        }
    }

    public String getBlockMathjaxEndIdentifier(){
        return this.m_blockMathjaxEndIdentifier;
    }
    public void setBlockMathjaxEndIdentifier( final String value ){
        if( StringUtils.isNotBlank( value )) {
            this.m_blockMathjaxEndIdentifier = value;
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                    pluginSettings.put(RestAdminConfigService.Config.class.getName() + "." + BLOCK_MATHJAX_END_IDENTIFIER, value);
                    return null;
                }
            });
        }
    }
}
