package com.keysight.mathjax.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.components.HtmlEscaper;
import com.keysight.mathjax.helpers.PluginHelper;

import java.util.Map;

public class MathJaxInline implements Macro {
    public final static String EQUATION = "equation";
    public final static String BODY_AS_HTML = "bodyAsHtml";
    protected final VelocityHelperService velocityHelperService;
    protected final XhtmlContent xhtmlUtils;
    protected final PluginHelper pluginHelper;

    public MathJaxInline(PluginHelper pluginHelper,
                         VelocityHelperService velocityHelperService,
                         XhtmlContent xhtmlUtils) {
        this.pluginHelper = pluginHelper;
        this.velocityHelperService = velocityHelperService;
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException {
        String equation = "No equation provided";
        String template = "/com/keysight/mathjax/templates/mathjax-inline.vm";
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();

        if (parameters.containsKey(EQUATION)) {
            //equation = "\\(" + HtmlEscaper.escapeAll(parameters.get(EQUATION), true) + "\\)";
            //equation = "(mathjax-inline(" + HtmlEscaper.escapeAll(parameters.get(EQUATION), true) + ")mathjax-inline)";
            equation = pluginHelper.getInlineMathjaxStartIdentifier() + HtmlEscaper.escapeAll(parameters.get(EQUATION), true) + pluginHelper.getInlineMathjaxEndIdentifier();
        }

        velocityContext.put(BODY_AS_HTML, equation);

        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }
}
